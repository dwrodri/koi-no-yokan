from datetime import datetime
from string import Template

import numpy as np
import pandas as pd
from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from fuzzywuzzy import fuzz
from jikanpy import Jikan
from scipy import sparse
from tqdm import tqdm
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.preprocessing import normalize

# load anime metadata
metadata = (
    pd.read_pickle("mini_mal.pkl.gz").drop_duplicates(subset=["title"]).sort_index()
)  # type: pd.DataFrame
# Useful constants
genres = [
    "action",
    "adventure",
    "cars",
    "comedy",
    "dementia",
    "demons",
    "mystery",
    "drama",
    "ecchi",
    "fantasy",
    "game",
    "hentai",
    "historical",
    "horror",
    "kids",
    "magic",
    "martial",
    "mecha",
    "music",
    "parody",
    "samurai",
    "romance",
    "school",
    "sci-fi",
    "shoujo",
    "shounen",
    "space",
    "sports",
    "super",
    "vampire",
    "yaoi",
    "yuri",
    "harem",
    "slice",
    "supernatural",
    "military",
    "police",
    "psychological",
    "thriller",
    "seinen",
    "josei",
]
SYNOPSIS_WORD_CAP = 70
MAX_RESULTS = 20
has_rec_filter = metadata["recommendation_ids"].apply(any)
data = metadata[has_rec_filter]["recommendation_counts"].explode().astype("int")
col_ind = (
    metadata[has_rec_filter]["recommendation_ids"].explode().astype("int")
)
row_ind = col_ind.index
print(f"{row_ind.max()}\t {len(row_ind)}")
print(f"{col_ind.max()}\t {len(row_ind)}")
print(f"{data.max()}\t {len(data)}")
rec_graph = sparse.csr_matrix(
    (data.values, (row_ind.values, col_ind.values)),
    shape=(
        row_ind.max()+1,
        row_ind.max()+1,
    ),
)
metadata = metadata.reset_index()
ez_titles = metadata["title"].str.lower()
review_vectorizer = TfidfVectorizer(
    analyzer="char",
    #  stop_words="english",
    lowercase=True,
    smooth_idf=True,
    ngram_range=(3, 3),
    min_df=2,
    max_df=0.95,
    token_pattern=r"(?u)\b[a-zA-Z_][a-zA-Z0-9_]+\b",
)
review_matrix = review_vectorizer.fit_transform(
    tqdm(
        metadata["reviews"].apply(lambda n: "".join(n)).values,
        unit="animes",
        desc="Vectorizing anime reviews...",
    )
)  # type: sparse.csr_matrix


jikan = Jikan()
app = FastAPI()


def load_file_as_string(filename: str) -> str:
    """
    Helper function for loading HTML
    """
    with open(filename, "r") as fp:
        return "".join(fp.readlines())


async def user_search(name: str) -> np.ndarray:
    """Given a username, return suggestion IDs in order."""
    result = jikan.user(username=name, request="animelist")
    selected_animes = [
        anime
        for anime in result["anime"]
        if (anime["watching_status"] == 2 or anime["watching_status"] == 6)
    ]
    query = [anime["mal_id"] for anime in selected_animes if anime["score"] > 0]
    weights = np.asarray_chkfinite(
        [anime["score"] for anime in selected_animes if anime["score"] > 0]
    )
    key_values = np.asarray(
        rec_graph[query].multiply(weights[:, None]).sum(axis=0)
    ).flatten()
    keys = np.array([key for key in key_values.nonzero()[0] if key not in query])
    ind = np.argsort(key_values[keys])[::-1]  # sort by value descending
    return metadata["mal_id"].searchsorted(keys[ind])

async def review_text_search(query: str) -> np.ndarray:
    # filter genres automatically
    # TODO: port bitmasking trick from indexr
    genre_matches = [
        i + 1 for i in range(len(genres)) if genres[i] in query.lower().split()
    ]
    genre_filter = metadata["genres"].apply(lambda n: any([genre in n for genre in genre_matches]))
    genre_matching_rows = metadata[genre_filter].index.values
    query_vec = normalize(review_vectorizer.transform([query]))
    search_space = (
        review_matrix[genre_matching_rows] if any(genre_matches) else review_matrix
    )
    key_values = np.asarray(search_space.dot(query_vec.transpose()).todense()).flatten()
    keys = (
        genre_matching_rows[np.argsort(key_values)[::-1][:MAX_RESULTS]]
        if genre_matches
        else np.argsort(key_values)[::-1][:MAX_RESULTS]
    )
    # Return near exact title matches
    title_matches = np.asarray([ez_titles.loc[ez_titles ==choice].index.values[0] for choice in ez_titles if fuzz.ratio(query.lower(),choice) > 91])
    return np.unique(np.hstack((title_matches,keys)))


@app.get("/", response_class=HTMLResponse)
async def return_homepage():
    return load_file_as_string("homepage.html")


@app.get("/search/", response_class=HTMLResponse)
async def initial_query(
    search_type: str = "Fulltext",
    userin: str = "",
):
    """
    Delegate search to corresponding handler, collect results and return HTML
    """
    start = datetime.now()
    keys = (
        await user_search(userin)
        if search_type == "user"
        else await review_text_search(userin)
    )
    body = Template(load_file_as_string("serp_body.html"))
    entry_template = Template(load_file_as_string("serp_entry.html"))
    entries = []
    for i in range(min([len(keys), MAX_RESULTS])):
        if keys[i] in metadata["mal_id"]:
            cleaned_synopsis = metadata.at[keys[i], "synopsis"]
            if (
                cleaned_synopsis is not None
                and len(cleaned_synopsis.split()) > SYNOPSIS_WORD_CAP
            ):
                cleaned_synopsis = (
                    " ".join(cleaned_synopsis.split()[:SYNOPSIS_WORD_CAP]) + "..."
                )
            entries.append(
                entry_template.substitute(
                    page_link=metadata.at[keys[i], "url"],
                    img_link=metadata.at[keys[i], "image_url"],
                    title=metadata.at[keys[i], "title"],
                    synopsis=cleaned_synopsis,
                )
            )
    duration = datetime.now() - start
    print(f"Query took: {duration.total_seconds()}")
    return body.substitute(output='<br style="padding=8rem">'.join(entries))
