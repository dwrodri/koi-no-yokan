# Koi-no-yokan (恋の予感)

A tool for finding Anime that you're bound to love.

## TODO

- Add reviews to dataset
- Add TF-IDF search for review text
- Weight TF-IDF search by PageRank graph weights