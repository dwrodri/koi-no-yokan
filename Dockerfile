FROM python:3.8-buster


WORKDIR /app/
COPY Pipfile /app
COPY main.py /app
COPY serp_body.html /app
COPY serp_entry.html /app
COPY homepage.html /app
COPY mal_dataset.pkl.gz /app
RUN pip install pipenv
RUN pipenv install
EXPOSE 80/tcp
CMD ["pipenv", "run", "uvicorn", "main:app", "--host", "0.0.0.0", "--port", "80"]